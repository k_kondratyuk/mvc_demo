﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using WebTest1.Models;

namespace WebTest1.Controllers
{
    public class LoginController : Controller
    {
        /// <summary>
        /// Информация для авторизации
        /// </summary>
        public class LoginData
        {
            /// <summary>
            /// Логин
            /// </summary>
            public string login { get; set; }

            /// <summary>
            /// Пароль
            /// </summary>
            public string password { get; set; }
        }

        //
        // GET: /Login/
        public ActionResult Index()
        {
            var temp = PasswordHash.CreateHash("user");
            if (MySession.Current.CanOpenList)
                return RedirectToAction("Index", "Home");

            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginData entity)
        {
            databaseEntities1 db = new databaseEntities1();
            var temp = db.account.Where(p => p.account_login == entity.login).FirstOrDefault();
            if (temp != null)
            {
                if (PasswordHash.ValidatePassword(entity.password, temp.account_password))
                {
                    //успешно
                    MySession.Current.UserAccount = temp;
                    return RedirectToAction("Index", "Home", new { type = "success" });
                }
            }

            ViewBag.Message = "Таких данных не найдено!";
            return View();
        }

        /// <summary>
        /// Хеширование паролей при помощи PBKDF2-SHA1.
        /// </summary>
        public class PasswordHash
        {
            // параметры вспомогательных данных
            public const int SALT_BYTE_SIZE = 24;
            public const int HASH_BYTE_SIZE = 24;
            public const int PBKDF2_ITERATIONS = 1000;

            public const int ITERATION_INDEX = 0;
            public const int SALT_INDEX = 1;
            public const int PBKDF2_INDEX = 2;

            /// <summary>
            /// Создание "посоленных" PBKDF2 хешей паролей
            /// </summary>
            /// <param name="password">Исходный пароль</param>
            /// <returns>Хеш пароля</returns>
            public static string CreateHash(string password)
            {
                // Создание случайной соли
                RNGCryptoServiceProvider csprng = new RNGCryptoServiceProvider();
                byte[] salt = new byte[SALT_BYTE_SIZE];
                csprng.GetBytes(salt);

                // Хеширование пароля с параметрами
                byte[] hash = PBKDF2(password, salt, PBKDF2_ITERATIONS, HASH_BYTE_SIZE);
                return PBKDF2_ITERATIONS + ":" +
                    Convert.ToBase64String(salt) + ":" +
                    Convert.ToBase64String(hash);
            }

            /// <summary>
            /// Проверка пароля на корректность
            /// </summary>
            /// <param name="password">Входной пароль</param>
            /// <param name="correctHash">Хеш корретного пароля</param>
            /// <returns>True, если пароль корректный</returns>
            public static bool ValidatePassword(string password, string correctHash)
            {
                // Extract the parameters from the hash
                char[] delimiter = { ':' };
                string[] split = correctHash.Split(delimiter);
                int iterations = Int32.Parse(split[ITERATION_INDEX]);
                byte[] salt = Convert.FromBase64String(split[SALT_INDEX]);
                byte[] hash = Convert.FromBase64String(split[PBKDF2_INDEX]);

                byte[] testHash = PBKDF2(password, salt, iterations, hash.Length);
                return SlowEquals(hash, testHash);
            }

            /// <summary>
            /// Сравнивает два массивы байтов в продолжительном времени. 
            /// Метод используется таким образом, что хэши паролей, не могут быть извлечены из
            /// онлайн-системы, использующие временную атаку.
            /// </summary>
            /// <param name="a">Первый массив байтов</param>
            /// <param name="b">Второй массив байтов</param>
            /// <returns>True если оба массива равны</returns>
            private static bool SlowEquals(byte[] a, byte[] b)
            {
                uint diff = (uint)a.Length ^ (uint)b.Length;
                for (int i = 0; i < a.Length && i < b.Length; i++)
                    diff |= (uint)(a[i] ^ b[i]);
                return diff == 0;
            }

            /// <summary>
            /// Вычисление PBKDF2-SHA1 хеша
            /// </summary>
            /// <param name="password">Пароль</param>
            /// <param name="salt">Соль</param>
            /// <param name="iterations">Количество итераций PBKDF2</param>
            /// <param name="outputBytes">Длина хеша в байтах</param>
            /// <returns>Хеш пароля</returns>
            private static byte[] PBKDF2(string password, byte[] salt, int iterations, int outputBytes)
            {
                Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(password, salt);
                pbkdf2.IterationCount = iterations;
                return pbkdf2.GetBytes(outputBytes);
            }
        }
    }
}