﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebTest1.Models;

namespace WebTest1.Controllers
{
    public class SearchController : Controller
    {
        private databaseEntities1 db = new databaseEntities1();

        //
        // GET: /AjaxTest/
        [HttpGet]
        public ActionResult Index()
        {
            if (!MySession.Current.CanUseSearch)
            {
                return RedirectToAction("Error", "Home");
            }
            return View();
        }

        /// <summary>
        /// Searching employers without filters
        /// </summary>
        /// <param name="input">Input string</param>
        /// <returns>Employers</returns>
        [HttpPost, ActionName("SimpleSearch")]
        public PartialViewResult Index(string input)
        {
            var found = db.employee.Where(p => (p.employee_firstname + p.employee_name + p.employee_lastname).Contains(input)).Take(10).ToList();
            return PartialView("Search", found);
        }

        [HttpPost, ActionName("Filters")]
        public PartialViewResult Index(string input, string min_s, string max_s, string min_d, string max_d)
        {
            DateTime t;
            DateTime tt;

            var q = int.Parse(min_s);
            var qq = int.Parse(max_s);

            if ((!DateTime.TryParseExact(min_d, "M/d/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out t)) ||
                (!DateTime.TryParseExact(max_d, "M/d/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out tt)))
            {
                var found = db.employee.Where(p => (p.employee_firstname + p.employee_name + p.employee_lastname).Contains(input)).
                    Where(p => p.employee_money >= q && p.employee_money <= qq).Take(10).ToList();
                return PartialView("Search", found);
            }
            else
            {
                

                var found = db.employee.Where(p => (p.employee_firstname + p.employee_name + p.employee_lastname).Contains(input)).
                   Where(p => p.employee_money >= q && p.employee_money <= qq).
                    Where(p => p.employee_birthday.Month * 100 + p.employee_birthday.Day > t.Month * 100 + t.Day).
                    Where(p => p.employee_birthday.Month * 100 + p.employee_birthday.Day < tt.Month * 100 + tt.Day).
                        Take(10).ToList();
                return PartialView("Search", found);
            }
        }
    }
}