﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebTest1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string type = "empty")
        {
            if (type == "success")
            {
                ViewBag.message = @"
                <script type=""text/javascript"">
                    $(document).ready(function () {
                        $.growl({ title: ""Добро пожаловать!"", message: "" "" });
                    });
                </script>";
            }
            return View(MySession.Current);
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}