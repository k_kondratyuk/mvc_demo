﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace WebTest1.Models
{
    public class EmployeeController : Controller
    {
        private databaseEntities1 db = new databaseEntities1();

        // GET: /Employee/
        public ActionResult Index(string type = "empty")
        {
            if (!MySession.Current.CanOpenList)
                return RedirectToAction("Error", "Home");

            if (type == "edit")
            {
                ViewBag.message = @"
                <script type=""text/javascript"">
                    $(document).ready(function () {
                        $.growl({ title: ""Поздравляем!"", message: ""Запись была отредактирована"" });
                    });
                </script>";
            }

            if (type == "delete")
            {
                ViewBag.message = @"
                <script type=""text/javascript"">
                    $(document).ready(function () {
                        $.growl({ title: ""Сожалеем!"", message: ""Запись была удалена"" });
                    });
                </script>";
            }

            if (type == "create")
            {
                ViewBag.message = @"
                <script type=""text/javascript"">
                    $(document).ready(function () {
                        $.growl({ title: ""Поздравляем!"", message: ""Запись была добавлена"" });
                    });
                </script>";
            }

            var employee = db.employee.Include(e => e.department);
            return View(employee.ToList());
        }

        // GET: /Employee/Details/5
        public ActionResult Details(long? id)
        {
            if (!MySession.Current.CanUseSearch)
                return RedirectToAction("Error", "Home");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employee employee = db.employee.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: /Employee/Create
        public ActionResult Create()
        {
            if (!MySession.Current.CanCRUD)
                return RedirectToAction("Error", "Home");

            ViewBag.employee_ref_department = new SelectList(db.department, "department_id", "department_title");
            return View();
        }

        // POST: /Employee/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "employee_id,employee_firstname,employee_name,employee_lastname,employee_birthday,employee_ref_department,employee_datestart,employee_money")] employee employee, HttpPostedFileBase file)
        {
            // Verify that the user selected a file
            if (file != null && file.ContentLength > 0)
            {
                byte[] picture = new byte[file.ContentLength];
                file.InputStream.Read(picture, 0, file.ContentLength);
                employee.employee_photo = picture;
            }

            if (ModelState.IsValid)
            {
                db.employee.Add(employee);
                db.SaveChanges();

                return RedirectToAction("Index", new { type = "create" });
            }

            ViewBag.employee_ref_department = new SelectList(db.department, "department_id", "department_title", employee.employee_ref_department);


            return View(employee);
        }

        // GET: /Employee/Edit/5
        public ActionResult Edit(long? id)
        {
            if (!MySession.Current.CanCRUD)
                return RedirectToAction("Error", "Home");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employee employee = db.employee.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.employee_ref_department = new SelectList(db.department, "department_id", "department_title", employee.employee_ref_department);
            return View(employee);
        }

        // POST: /Employee/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "employee_id,employee_firstname,employee_name,employee_lastname,employee_birthday,employee_ref_department,employee_datestart,employee_money,employee_photo")] employee employee,
            HttpPostedFileBase file, string remove)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;

                if (remove == "remove")
                {
                    employee.employee_photo = null;
                }
                else
                    if (file != null && file.ContentLength > 0)
                    {
                        byte[] picture = new byte[file.ContentLength];
                        file.InputStream.Read(picture, 0, file.ContentLength);
                        employee.employee_photo = picture;
                    }
                    else
                    {
                        employee.employee_photo = db.employee.Where(p => p.employee_id == employee.employee_id).FirstOrDefault().employee_photo;
                        db.Entry(employee).Property(t => t.employee_photo).IsModified = false;
                    }

                
                db.SaveChanges();
                return RedirectToAction("Index", new { type = "edit"});

            }
            ViewBag.employee_ref_department = new SelectList(db.department, "department_id", "department_title", employee.employee_ref_department);
            return View(employee);
        }

        // GET: /Employee/Delete/5
        public ActionResult Delete(long? id)
        {
            if (!MySession.Current.CanCRUD)
                return RedirectToAction("Error", "Home");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            employee employee = db.employee.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: /Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            employee employee = db.employee.Find(id);
            db.employee.Remove(employee);
            db.SaveChanges();

            return RedirectToAction("Index", new { type = "delete" });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
