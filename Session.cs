﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebTest1
{
    public class MySession
    {
        // private constructor
        private MySession()
        {
            UserRights = new BitArray(16);
        }

        // Gets the current session.
        public static MySession Current
        {
            get
            {
                MySession session = (MySession)HttpContext.Current.Session["__MySession__"];
                if (session == null)
                {
                    session = new MySession();
                    HttpContext.Current.Session["__MySession__"] = session;
                }
                return session;
            }
        }

        private WebTest1.Models.account _userAccount { get; set; }

        public WebTest1.Models.account UserAccount
        {
            get
            {
                return _userAccount;
            }
            set
            {
                UserRights = new BitArray(new int[] { unchecked((int)value.usergroup.usergroup_rights) });
                _userAccount = value;
            }

        }

        /// <summary>
        /// Can open the list of all employeers
        /// </summary>
        public bool CanOpenList
        {
            get
            {
                return UserRights[0];
            }
        }

        public bool CanUseSearch
        {
            get
            {
                return UserRights[1];
            }
        }

        public bool CanCRUD
        {
            get
            {
                return UserRights[2];
            }
        }

        public BitArray UserRights { get; set; }
        
    }
}